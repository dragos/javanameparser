package com.elsvr;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class NameParser {

    /**
     * Parse a string encoded name
     * @param inputName a string encoded name
     * @return a name object
     * @throws IllegalArgumentException when the input is not valid
     */
    public Name parseSingleName(String inputName) throws IllegalArgumentException {

        Name fullName = new Name();

        if (inputName == null)
            throw new IllegalArgumentException("The input cannot be null");

        inputName = inputName.trim();

        int comaIndex = inputName.indexOf(",");
        int spaceIndex = inputName.lastIndexOf(" ");

        if (comaIndex != -1) {
            fullName.setLastName(inputName.substring(0, comaIndex).trim());
            fullName.setFirstName(inputName.substring(comaIndex + 1, inputName.length()).trim());
        } else if (spaceIndex != -1) {
            fullName.setLastName(inputName.substring(spaceIndex).trim());
            fullName.setFirstName(inputName.substring(0, spaceIndex).trim());
        } else
            throw new IllegalArgumentException("The input does not seem to be a proper name");

        return fullName;
    }

    /**
     * Parse a string encoded name list separated by a comma
     * @param inputList a string encoded name list
     * @return a list of name objects
     * @throws IllegalArgumentException when the input is not valid
     */
    public List<Name> parseMultipleNames(String inputList) throws IllegalArgumentException {
        return parseMultipleNames(inputList, ",");
    }

    /**
     * Parse a string encoded name list separated by a custom separator
     * @param inputList a string encoded name list
     * @param separator a custom separator
     * @return a list of name objects
     * @throws IllegalArgumentException when the input is not valid
     */
    public List<Name> parseMultipleNames(String inputList, String separator) throws IllegalArgumentException {
        if (inputList == null)
            throw new IllegalArgumentException("The input cannot be null");

        inputList = inputList.trim();

        List<String> nameList = Arrays.asList(inputList.split(separator));
        List<Name> names = new ArrayList<>();

        for (String n : nameList) {
            names.add(parseSingleName(n.trim()));
        }
        return names;
    }

}
