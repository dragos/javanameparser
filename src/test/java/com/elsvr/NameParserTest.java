package com.elsvr;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class NameParserTest {

    private NameParser testParser;

    @Before
    public void BeforeTest() {
        testParser = new NameParser();
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSingleNameNullCase() {
        testParser.parseSingleName(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSingleNameEmptyCase() {
        testParser.parseSingleName("");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSingleNameIncompleteCase() {
        testParser.parseSingleName("Jensen");
    }

    @Test
    public void testSingleNameParser() {
        Name name1 = testParser.parseSingleName("John Doe");
        Assert.assertEquals("John", name1.getFirstName());
        Assert.assertEquals("Doe", name1.getLastName());

        Name name2 = testParser.parseSingleName("Doe, John");
        Assert.assertEquals("John", name2.getFirstName());
        Assert.assertEquals("Doe", name2.getLastName());

        Name name3 = testParser.parseSingleName("Hans-Christian Jensen");
        Assert.assertEquals("Hans-Christian", name3.getFirstName());
        Assert.assertEquals("Jensen", name3.getLastName());

        Name name4 = testParser.parseSingleName("P. H. Kristensen");
        Assert.assertEquals("P. H.", name4.getFirstName());
        Assert.assertEquals("Kristensen", name4.getLastName());

        Name name5 = testParser.parseSingleName("Kristensen, P. H.");
        Assert.assertEquals("P. H.", name5.getFirstName());
        Assert.assertEquals("Kristensen", name5.getLastName());

        Name name6 = testParser.parseSingleName("Peter Hans Kristensen");
        Assert.assertEquals("Peter Hans", name6.getFirstName());
        Assert.assertEquals("Kristensen", name6.getLastName());

        Name name7 = testParser.parseSingleName("Peter H. Kristensen");
        Assert.assertEquals("Peter H.", name7.getFirstName());
        Assert.assertEquals("Kristensen", name7.getLastName());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testListNameNullCase() {
        testParser.parseMultipleNames(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testListNameEmptyCase() {
        testParser.parseMultipleNames("");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testListNameIncompleteCase() {
        testParser.parseMultipleNames("Jensen");
    }

    @Test
    public void testSingleListNameCase() {
        List<Name> parsNames = testParser.parseMultipleNames("H-C Jensen");
        Assert.assertEquals(1, parsNames.size());

        Assert.assertEquals("Jensen", parsNames.get(0).getLastName());
        Assert.assertEquals("H-C", parsNames.get(0).getFirstName());
    }

    @Test
    public void testListNameParser() {
        List<Name> authNames = new ArrayList<Name>() {{
            add(new Name("H-C", "Jensen"));
            add(new Name("Peter Hans", "Kristensen"));
            add(new Name("John", "Doe"));
            add(new Name("J.J", "Abrams"));
        }};

        List<Name> parsNames = testParser.parseMultipleNames("H-C Jensen, Peter Hans Kristensen, John Doe,J.J Abrams");

        Assert.assertEquals(authNames.size(), parsNames.size());

        for(int i=0; i<authNames.size(); i++){
            Assert.assertEquals(authNames.get(i).getLastName(), parsNames.get(i).getLastName());
            Assert.assertEquals(authNames.get(i).getFirstName(), parsNames.get(i).getFirstName());
        }
    }

}